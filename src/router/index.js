import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import AddTestimony from "../views/AddTestimony.vue";
import Contact from "../views/Contact.vue";
import Faq from "../views/Faq.vue";
import IAm from "../views/IAm.vue";
import ListTestimony from "../views/ListTestimony.vue";
import Project from "../views/Project.vue";
import ProximitySolution from "../views/ProximitySolution.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/addTestimony",
    name: "addTestimony",
    component: AddTestimony
  },
  {
    path: "/contact",
    name: "contact",
    component: Contact
  },
  {
    path: "/faq",
    name: "faq",
    component: Faq
  },
  {
    path: "/Iam",
    name: "Iam",
    component: IAm
  },
  {
    path: "/listTestimony",
    name: "listTestimony",
    component: ListTestimony
  },
  {
    path: "/project",
    name: "project",
    component: Project
  },
  {
    path: "/proximitySolution",
    name: "proximitySolution",
    component: ProximitySolution
  }
];

const router = new VueRouter({
  routes
});

export default router;
